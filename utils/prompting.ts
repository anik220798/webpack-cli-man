import chalk from 'chalk';
export const usageMessage = () => {
    console.log(`
    This Is a Manual For webpack config file

    ${chalk.yellowBright("$ webpack-cli man [webpack config name]")}

    example command:-

    ${chalk.yellowBright("$ webpack-cli man plugin")}

            `)
}



export const showManual = (manualFor, message) => {
    console.log(
        chalk.inverse.bold("Property Name : "+manualFor)+"\n",
        chalk.blue(message)
    )
}