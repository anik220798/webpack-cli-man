import manualsObj from "./manualList"
import {
    stringBeautifier
} from "./helper"
export const getManual = (property: string) => {
    let msg = manualsObj[
        Object.keys(manualsObj).find(
            p =>
            p.toString().trim().toLowerCase() == property.toString().trim().toLowerCase()
        )
    ] || "Not Found"

    msg = stringBeautifier(msg)
    return msg;

}