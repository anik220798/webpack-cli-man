import chalk from 'chalk';


export const stringBeautifier = (msg:string) => {
    // For codes
    let MsgArray = msg.split("---")

    MsgArray.map((m,i) => {

        if( i%2 == 1){
            MsgArray[i] = `${chalk.yellowBright(m)}`
        }
    })

    // For Quotes
    let quotesTempMsgArray = MsgArray.join("").split(">>>")
    quotesTempMsgArray
    .map((q,i) => {
        if( i%2 == 1){

            quotesTempMsgArray[i] = `${chalk.inverse.bold.italic(q)}`
        }
    })

    return quotesTempMsgArray.join("")




}