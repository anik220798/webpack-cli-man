export default {
  "Multiple_bundles": `Export multiple bundles by setting the output to [name].js. This example produces a.js and b.js.

    module.exports = {
      entry: {
        a: './a',
        b: './b'
      },
      output: { filename: '[name].js' }
    }
    Concerned about duplication? Use the CommonsChunkPlugin to move the common parts into a new output file.

    plugins: [ new webpack.optimize.CommonsChunkPlugin('init.js') ]
    <script src='init.js'></script>
    <script src='a.js'></script>`,
  "Entry": `
    An entry point indicates which module webpack should use to begin building out its internal dependency graph. webpack will figure out which other modules and libraries that entry point depends on (directly and indirectly).

    By default its value is ./src/index.js, but you can specify a different (or multiple entry points) by configuring the entry property in the webpack configuration. For example:

    webpack.config.js

    module.exports = {
    entry: './path/to/my/entry/file.js'
    };
    `,
  "Output": `
    The output property tells webpack where to emit the bundles it creates and how to name these files. It defaults to ./dist/main.js for the main output file and to the ./dist folder for any other generated file.

    You can configure this part of the process by specifying an output field in your configuration:

    webpack.config.js
    ---
    const path = require('path');
    ---
    ---
    module.exports = {
      entry: './path/to/my/entry/file.js',
      output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'my-first-webpack.bundle.js'
      }
    };
    ---
    In the example above, we use the output.filename and the output.path properties to tell webpack the name of our bundle and where we want it to be emitted to. In case you're wondering about the path module being imported at the top, it is a core Node.js module that gets used to manipulate file paths.
    `

}