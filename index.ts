import chalk from "chalk";
// import * as inquirer from "inquirer";
// import * as path from "path";
// import { List } from "@webpack-cli/webpack-scaffold";
import * as process from "process"
import {
    usageMessage,
    showManual
} from "./utils/prompting"
import {
    getManual
} from "./utils/manual"


export const parsingArgument = () => {
    const {
        argv
    } = process;
    if (argv.length < 3) {
        usageMessage();
        return 0;
    }
    const propertyName = process.argv.pop().replace(/[\-]+/, "").trim();
    const manualMsg = getManual(propertyName);
    showManual(propertyName,manualMsg)
}



(parsingArgument())